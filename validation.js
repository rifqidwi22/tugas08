document.getElementById('loginForm').addEventListener('submit', function(event) {
    event.preventDefault();

    let username = document.getElementById('input_text').value.trim();
    let password = document.getElementById('input_password').value.trim();
    let nim = document.getElementById('input_nim').value.trim();
    let errorMsg = document.getElementById('error_msg');

    function showError(message) {
        errorMsg.style.display = 'block';
        errorMsg.textContent = message;
    }

    if (username === "") {
        showError('Nama wajib diisi!');
    } else if (password === "") {
        showError('Password wajib diisi!');
    } else if (nim === "") {
        showError('NIM wajib diisi!');
    } else {
        errorMsg.style.display = 'none';
        alert('Login sukses!');
    }
});
